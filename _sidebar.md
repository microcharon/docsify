<!-- docs/_sidebar.md -->

* **基本文档**
  * [关于Wiki](/ "Wikicharon")
  * [站点重大事件簿](/whats-new)
  * [指南](/guide)
* **技术手册**
  * [robots.txt的写法规则及相关语法](/tech/robots-txt)
  * [CSS样式表的几种类型](/tech/types-of-css)
  * [标题命名格式及标准化](/tech/name-title)
  * [通过rclone备份网站及数据库到你的云端硬盘](/tech/rclone-backup-to-drive)
  * [Telegram：新手入门,使用教程](/tech/telegram-to-start)
* **命令手册**
  * [Git基础](/command/git-basic)
  * [最常用的主要rclone命令](/command/rclone)
  * [Docker常用命令](/command/docker)
* **Linux专题**
  * [Linux常用命令: zip, unzip 压缩和解压缩命令](/linux/how-to-use-zip-and-unzip)
  * [vi/vim 的使用](/linux/begin-with-vim)
* **杂文阅读**
  * [个人存档](/readings/archive)
  * [The Charm of Chinese Character](/readings/the-charm-of-chinese-character)
