# CSS样式表的几种类型

## 内联式

内联类型的样式表是指与样式有关的信息被功能化到现有的HTML元素中。使用内联方式，而不是一次性定义你的样式，你必须在你用来设计网页的每一个HTML元素中写下这个样式。它可以更准确地称为内联样式而不是内联样式表。它使用该HTML元素中的样式属性。

语法：

```html
<HTML ELEMENT style="properties: value"> .... </HTML ELEMENT>
```

示例：

```html
<p style="color: blue">The text gets the effect of inline style.</p>
```

内联式的缺点

- 使用内联样式并不像CSS中其他类型的样式表那样强大。如果你为许多HTML标签定义样式，而这些标签是用来设计网页的，那么你将无法使用与CSS相关的大部分功能。
- 例如，每当使用时，你必须重复定义样式，而不是只定义一次。
- 此外，如果你想改变一个特定的样式，你必须在文档中的许多地方改变它，而不是把它改变到一个地方。

## 嵌入式

嵌入样式表也是一种样式表，设计者可以通过使用以下方法将样式表的信息嵌入到HTML文档中

```html 
<style>
```
的元素中。这种将样式表信息嵌入到
```html 
<style> .... </style>
```
标签是在HTML的标题部分完成的

嵌入样式表的语法没有这种例外。简单地说，你必须把样式表的代码放在 

```html
<head>.....</head>
```

嵌套在head元素中的

```html
<style> .... </style>
```

标签之间。

示例：
```html
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Embedded Style Sheets Example</title>
    <style>
        h1 {
            border-bottom: 1px solid #DDDDDD;
            color: #069;
            font-family: Helvetica, Arial;
            font-size: 25px;
            font-weight: normal;
            line-height: 34px;
            margin-bottom: 10px;
            outline: 0 none;
            padding-bottom: 3px;
            padding-top: 0;
            text-decoration: none;
        }
        
        hr {
            background-color: #069;
            border: 0 none;
            clear: both;
            color: #D4D4D4;
            height: 1px;
        }
        
        .sublines {
            background-color: #DAF4FE;
            padding: 5px;
            border: 1px solid #09C;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
        }
        
        .infotext {
            font-size: 10pt;
            background-color: #F2F2F2;
            padding: 5px;
        }
    </style>
</head>

<body>
    <h1><span class="headlines">Welcome to w3schools.in</span><br>
</h1>
    <div class="sublines"> This is an example page using CSS.
        <br> The example is really simple,
        <br> and doesn't even look good,
        <br> but it shows the technique. </div>
    <br>
    <table border="0" cellpadding="3" cellspacing="1">
        <tr>
            <td class="sublines"> As you can see:
                <br>
            </td>
            <td class="sublines">The styles even work on tables.</td>
        </tr>
    </table>

    <hr>

    <div class="infotext">Example from w3schools.in</div>
    <hr>
</body>

</html>
```
## 外部式

这种类型的样式表得到一个单独的文件，设计师可以在其中陈述每一个似乎与你的网站有关的CSS样式。然后，这必须与你的HTML页面的外部样式表联系起来。你必须遵循一些特定的步骤，使这个概念性的样式表可以实现。

创建外部样式表的步骤：

1. 通过在一个纯文本文件中输入CSS代码来建立样式表（通常使用文本编辑器），然后以.css扩展名保存。
2. 你必须使用一个HTML链接元素将样式表与HTML文档连接起来。

示例：

```html
<head> <link rel="stylesheet" href="style.css" /> </head>
```
