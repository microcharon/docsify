# 通过rclone备份网站及数据库到你的云端硬盘

下文皆以备份数据到Google Drive为例

## 本地客户端

### 下载并运行

![Rclone for Windows](_media/rclone-backup-to-drive/1.webp)

1. 下载对应的客户端	[Rclone downloads](https://rclone.org/downloads/)
2. 解压缩文件到一个偏保守的目录（不会经常访问的目录），如：C:\Windows\System32
3. 终端键入 cd C:\Windows\System32\rclone
4. rclone config

至此已开始运行rclone配置项目

### 获取服务器端所需要的token

![Rclone for Windows](_media/rclone-backup-to-drive/2.webp)

![Rclone for Windows](_media/rclone-backup-to-drive/3.webp)

1. 终端输入n新建remote
2. 这一步便是为新建的remote任意取个名字
3. 这一步要求我们选择对应的存储remote，选择你最终要备份到的那个选项，我这里选择18（Google Drive），记住选项应rclone的版本而异（命令行rclone version查询版本）
4. 下面要求填写client_id与client_secret（rclone建议填写我们自己设置的），这里我们直接不写（下文我会示范如何自定义配置这两项），回车跳过
5. 提示要哪一种授予权限，推荐默认选择1
6. 提示 Leading `~` will be expanded in the file name as will environment variables such as `${RCLONE_CONFIG_DIR}`.
   Enter a value. Press Enter to leave empty. 此处建议直接回车跳过
7. 提示是否编辑高级配置，这里选择默认就行，输入n
8. 提示是否自动设置，这里我们选择默认（与服务器端选择不同，请注意），输入y
9. 跳转授权页面，成功后得到我们后面服务器端所需要的token，复制范围{"access_token":…………………………2022-08-18…………………………"}
10. 接下来按照提示保存退出即可

![Rclone for Windows](_media/rclone-backup-to-drive/4.webp)

## 服务器端

### 下载并运行

1. `curl https://rclone.org/install.sh | sudo bash`
2. 出现下列语句即为安装成功 Now run "rclone config" for setup. Check https://rclone.org/docs/ for more details.
3. rclone config

### 输入服务器端所需要的token

1. 请参照本地客户端配置前七步做法，在第八步略有变化
2. 提示是否自动设置，这里我们选择No，输入n（因为服务器一般都是命令行操作）
3. 输入服务器端所需要的token
4. 完成剩余设置，退出

### 挂载

1. CentOS 系统下需要fuse才能挂载，没有则`yum install fuse`
2. `rclone mount DriveName:Folder LocalFolder --copy-links --no-gzip-encoding --no-check-certificate --allow-other --allow-non-empty --umask 000` DriveName替换为自定义的remote名，Folder替换为存储盘路径，LocalFolder替换为VPS上的路径，例如`rclone mount backup:/ /root/backup --copy-links --no-gzip-encoding --no-check-certificate --allow-other --allow-non-empty --umask 000`
3. 在aapanel或者宝塔面板的设置页面设置你的新备份数据路径，在定时任务（cron）里尝试运行并查看log，成功的话，你的Google Drive上便能看到上传的文件

## 高级配置

### 自定义client

自己看rclone文档

[Google Drive (rclone.org)](https://rclone.org/drive/#making-your-own-client-id)

[Google Cloud Console](https://console.cloud.google.com/apis/)
