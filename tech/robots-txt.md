# robots.txt的写法规则及相关语法

## 三个核心

### User-agent

定义一条规则所适用的搜索引擎，如Bingbot表示必应蜘蛛，Googlebot表示谷歌蜘蛛

### Allow

建议搜索引擎抓取和索引某个文件、页面或目录

### Disallow

建议搜索引擎不要抓取和索引某个文件、页面或目录

## 匹配符

$  结束符  [所有以他结尾的都能够进行匹配]

\* 匹配符  [匹配零或任意多个字符]

| Allow 或 Disallow 的值 | URL         | 匹配结果 |
| :--------------------- | :---------- | :------- |
| /tmp                   | /tmp        | yes      |
| /tmp                   | /tmp.html   | yes      |
| /tmp                   | /tmp/a.html | yes      |
| /tmp                   | /tmphoho    | no       |
| /Hello*                | /Hello.html | yes      |
| /He*lo                 | /Hello,lolo | yes      |
| /Heap*lo               | /Hello,lolo | no       |
| html$                  | /tmpa.html  | yes      |
| /a.html$               | /a.html     | yes      |
| htm$                   | /a.html     | no       |

## 实际应用

阻止任何搜索引擎从网站爬取任何文件、页面或目录

```ASCII
User-agent: *
Disallow: /
```

| 例 1. 禁止所有搜索引擎访问网站的任何部分                     | User-agent: * Disallow: /                                    |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| 例 2. 允许所有的 robot 访问 (或者也可以建一个空文件 “/robots.txt”) | User-agent: * Allow: /                                       |
| 例 3. 仅禁止 Baiduspider 访问您的网站                        | User-agent: Baiduspider Disallow: /                          |
| 例 4. 仅允许 Baiduspider 访问您的网站                        | User-agent: Baiduspider Allow: / User-agent: * Disallow: /   |
| 例 5. 仅允许 Baiduspider 以及 Googlebot 访问                 | User-agent: Baiduspider Allow: / User-agent: Googlebot Allow: / User-agent: * Disallow: / |
| 例 6. 禁止 spider 访问特定目录 在这个例子中，该网站有三个目录对搜索引擎的访问做了限制，即 robot 不会访问这三个目录。需要注意的是对每一个目录必须分开声明，而不能写成 “Disallow: /cgi-bin/ /tmp/”。 | User-agent: * Disallow: /cgi-bin/ Disallow: /tmp/ Disallow: /~joe/ |
| 例 7. 允许访问特定目录中的部分 url                           | User-agent: * Allow: /cgi-bin/see Allow: /tmp/hi Allow: /~joe/look Disallow: /cgi-bin/ Disallow: /tmp/ Disallow: /~joe/ |
| 例 8. 使用”*”限制访问 url 禁止访问/cgi-bin/目录下的所有以”.htm”为后缀的 URL(包含子目录)。 | User-agent: * Disallow: /cgi-bin/*.htm                       |
| 例 9. 使用”$”限制访问 url 仅允许访问以”.htm”为后缀的 URL。   | User-agent: * Allow: /*.htm$ Disallow: /                     |
| 例 10. 禁止访问网站中所有的动态页面                          | User-agent: * Disallow: /*?*                                 |
| 例 11. 禁止 Baiduspider 抓取网站上所有图片 仅允许抓取网页，禁止抓取任何图片。 | User-agent: Baiduspider Disallow: /*.jpg$ Disallow: /*.jpeg$ Disallow: /*.gif$ Disallow: /*.png$ Disallow: /*.bmp$ |
| 例 12. 仅允许 Baiduspider 抓取网页和.gif 格式图片 允许抓取网页和 gif 格式图片，不允许抓取其他格式图片 | User-agent: Baiduspider Allow: /*.gif$ Disallow: /*.jpg$ Disallow: /*.jpeg$ Disallow: /*.png$ Disallow: /*.bmp$ |
| 例 13. 仅禁止 Baiduspider 抓取.jpg 格式图片                  | User-agent: Baiduspider Disallow: /*.jpg$                    |

## 参考资料

[什么是Robots协议,标准写法是什么 - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1134866)

[robots.txt的正确用法 (zhanzhangb.com)](https://www.zhanzhangb.com/1808.html)

