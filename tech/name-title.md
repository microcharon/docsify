# 标题命名格式及标准化

()内为可选项，[]内标明备注，{}内为变量名

TIME_ZONE: EN, CN

| 环境常量及变量 | 描述         |
| -------------- | ------------ |
| ALBUM          | 专辑         |
| ARTIST(S)      | 艺术家       |
| DESCRIPTION    | 描述或是说明 |
| DISCOGRAPHY    | 合集         |
| FORMAT         | 格式         |
| LANGUAGE       | 语言         |
| TIME           | 公历时间     |
| VERSION        | 版本         |
| SOFT_NAME      | 软件名       |
| GAME_NAME      | 游戏名       |

## 软件类

{SOFT_NAME}[CN]+{SOFT_NAME}[EN]+{VERSION}+{DESCRIPTION}

{SOFT_NAME}+{DESCRIPTION}

## 音乐类

({DESCRIPTION})+{ARTIST(S)}+{ALBUM}+{TIME}+{FORMAT}

({DESCRIPTION})+{ARTIST(S)}+{TIME}+{DISCOGRAPHY}

## 游戏类

{GAME_NAME}[CN]+{GAME_NAME}[EN]+{VERSION}+{TIME}+{LANGUAGE}
