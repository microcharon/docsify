# Docker常用命令

### 查看本地镜像

```
docker images
```

### 删除本地镜像

```
docker rmi 镜像ID
```

### 查看运行中的容器

```
docker ps
```

### 查询所有容器

```
docker ps -a
```

### 停止容器

```
docker stop 容器ID或name
```

### 删除容器

```
docker rm 容器ID或name
```

### 查看容器日志

```
docker logs 容器ID或name
```