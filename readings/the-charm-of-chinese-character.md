# The Charm of Chinese Character

Today we'll deliver the speech about Chinese Characters - The Charm of Chinese Character. Chinese characters are logograms developed for the writing of Chinese which are the oldest continuously used system of writing in the world. By virtue of their widespread current use throughout East and Southeast Asia, as well as their profound historic use throughout the Sinosphere, Chinese characters are among the most widely adopted writing systems in the world by number of users.

今天我们将发表关于汉字的演讲--汉字的魅力。汉字是为书写中文而开发的逻辑图形，是世界上持续使用的最古老的书写系统。由于汉字目前在整个东亚和东南亚地区的广泛使用，以及其在整个汉字圈的深刻历史使用，汉字是世界上使用人数最多的书写系统之一。

## Origin

There are three versions of fairy tales about the origin of Chinese characters, namely, the eight diagrams theory, the changjie character making theory and the knot theory.

汉字的起源有三个版本的神话故事，分别是八卦说，仓颉造字还有结绳说。

- The Eight Diagrams - The eight diagrams symbolize the eight natural phenomena: **sky, earth, thunder, wind, water, fire, mountain and lake**. So, it represented the ancient Chinese early knowledge of the universe, which contained a simple dialectical materialist point of view.（八卦图象征着八个自然现象：天、地、雷、风、水、火、山、湖。所以，它代表了中国古代对宇宙的早期认识，其中包含了简单的辩证唯物主义观点。）
- The Changjie Character making - Chinese characters were invented by a man named Cangjie, who was an official during the time of the legendary Yellow Emperor, Huangdi.（汉字是由一个叫仓颉的人发明的，他是传说中的黄帝时代的一个官员。）
- Knotting - Knotting refers to one of the means by which humans in ancient times, free from the constraints of time and space, recorded facts and spread them. It took place long after the creation of language and before the appearance of writing.（结绳记事，指远古时代人类，摆脱时空限制记录事实、进行传播的一种手段之一。它发生在语言产生以后、文字出现之前的漫长年代里。）

## Revolution (Development)

There are six main stages in the development of Chinese characters. They are oracle bone script, golden script, seal script, official script, regular script and cursive script.

汉字的发展有六个阶段。依次是甲骨文、金文、篆书、隶书、楷书、草书。

- Oracle Bone Script - The earliest of Chinese characters during the *Shang* Dynasty (1600 - 1046 B.C.)
- Golden Script - Made during the *Shang* (1600 – 1046 B.C.) and *Zhou* (1046 – 256 B.C.) dynasties
- Seal Script - The written language popular during the *Qin* Dynasty (221-207 B.C.)
- Official Script - The formal written language of the *Han* Dynasty (206 B.C. – 220 A.D.)
- Regular Script - First appeared at the end of the *Han* Dynasty. But it was not until the Southern and Northern Dynasties (420-589 A.D.) that Regular Script rose to dominant status
- Cursive Writing - First appeared at the beginning of the *Han* Dynasty, Cursive Writing is not in general use, being a purely artistic, calligraphic style

## Structure (Conformation)

The conformations of Chinese characters include pictographs, self-explanatory character, associative components, picto-phonetic characters, phonetic loan characters and synonymous character.

汉字的构象包括象形、指事、会意、形声、假借、转注。

- Pictographs - It means "illustration of a shape"
- Self-explanatory Character - It means "pointing at things"
- Associative Components - It means "assembled meanings"
- Picto-phonetic Characters - It means "combination of form and                                                                                                               sound."
- Phonetic Loan Characters - It means "making use of" that are "borrowed" to write another homophonous or near-homophonous morpheme
- Synonymous Character - It means "the same meaning to another character"

## Feature (Characteristic)

Chinese characters are block characters and detachable ideograms.

汉字是可拆分的方块字和表意文字。

Just as Roman letters have a characteristic shape (lower-case letters mostly occupying the x-height, with ascenders or descenders on some letters), Chinese characters occupy a more or less square area in which the components of every character are written to fit in order to maintain a uniform size and shape, especially with small printed characters in Ming and sans-serif styles. Because of this, beginners often practise writing on squared graph paper, and the Chinese sometimes use the term "Square-Block Characters" (方块字 / 方塊字, fāngkuàizì), sometimes translated as tetragraph, in reference to Chinese characters.

正如罗马字母有其特有的形状（小写字母大多占据X高度，有些字母有升格或降格），汉字或多或少占据一个方形区域，为了保持统一的大小和形状，每个字的组成部分都要写在其中，尤其是明体和无衬线体的小印刷字。正因为如此，初学者经常在方形的图画纸上练习书写，中国人有时会用 "方块字"（fāngkuàizì）一词来指代汉字，有时也翻译为四角形。

## Summary

Chinese is the oldest continuously spoken language in the world. has its own charm.

汉语是世界上最古老的持续使用的语言，有其独特的魅力。

It is beautiful, in its shape and meaning. The Chinese writing system also has far greater aesthetic interest than simple scripts.

它是美丽的，在它的形状和意义上。 中国文字系统也比简单的文字具有更大的审美趣味。

It usually contains the wisdom of how people understand the concept.

 它通常包含了人们理解概念的智慧。

The Chinese is the most powerful language and we believe it can spread through the world in the future!

中文是最强大的语言，我们相信它在未来可以传遍世界！

Finally, thank your presence will be with us forever like this in coming days. And that's all the speech, thank you again!

最后，感谢您的与会将在未来的日子里永远与我们同在。 以上就是演讲的全部内容，再次感谢您！
