# 个人存档

Archive for Microcharon, personal use only.

## 必看文档

[Alist Document (nn.ci)](https://alist-doc.nn.ci/)

[Built-in Functions — Python 3.10.6 documentation](https://docs.python.org/3/library/functions.html)

[Clash for Windows | Clash for Windows (lbyczf.com)](https://docs.cfw.lbyczf.com/)

[docsify](https://docsify.js.org/#/)

[开发人员工具、技术文档和代码示例 | Microsoft Docs](https://docs.microsoft.com/zh-cn/)

## 辅助学习

[Reeborg's World](https://reeborg.ca/reeborg.html)

[Regex Learn](https://regexlearn.com/zh-cn)

## 文章收集

[手把手教你搭建私人离线下载神器Aria2，搭配Rclone+Filebrowser自动上传至网盘 (laoda.de)](https://blog.laoda.de/archives/aria2-rclone-filebrowser#安装docker)

[Aria2 + Rclone 实现 OneDrive、Google Drive 等网盘离线下载 - P3TERX ZONE](https://p3terx.com/archives/offline-download-of-onedrive-gdrive.html)

[Aria2 无法下载磁力链接、BT种子和速度慢的解决方案 - P3TERX ZONE](https://p3terx.com/archives/solved-aria2-cant-download-magnetic-link-bt-seed-and-slow-speed.html)

[p3terx/ariang - Docker Image | Docker Hub](https://hub.docker.com/r/p3terx/ariang)

[让Typecho支持上传Webp格式的图片 - 隔壁老李-私人博客 (mchsfc.com)](https://www.mchsfc.com/97.html)

[Bootstrap 图标库 · Bootstrap 官方开源图标（icon）库 (bootcss.com)](https://icons.bootcss.com/)

[Bing每日壁纸档案库，附API接口及PHP源码-博客建站-博客联盟用户社区 (blorg.cn)](https://discuss.blorg.cn/thread-52.htm)

[【必应】Bing自动提交收录python脚本 - 初的小站 (songonline.top)](https://www.songonline.top/archives/192/)

[针对Handsome主题做的SEO优化-陶小桃Blog (52txr.cn)](https://www.52txr.cn/2022/handsomeSEO.html)

[Nginx 的 location 匹配 (chenhe.me)](https://chenhe.me/post/nginx-location-match/)

[【教程】Genshin签到小助手部署 | Soraの摸鱼站 (sorami.top)](https://www.sorami.top/2022/03/16/Genshin签到小助手部署教程/)
